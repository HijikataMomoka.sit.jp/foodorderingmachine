import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMashineGUI {
    private JPanel root;
    private JButton button_hamburg;
    private JButton button_gyudon;
    private JButton button_karaage;
    private JButton button_curry;
    private JButton button_nikujyaga;
    private JButton button_shougayaki;


    private JTextArea orderedItemsList;
    private JButton checkoutbutton;
    private int totalprice = 0;
    private JLabel pricefield;

    public FoodOrderingMashineGUI() {
        button_shougayaki.setIcon(new ImageIcon(this.getClass().getResource("shougayaki.jpg")));
        button_hamburg.setIcon(new ImageIcon(this.getClass().getResource("hamburg.jpg") ));
        button_curry.setIcon(new ImageIcon(this.getClass().getResource("curryrice.jpg") ));
        button_nikujyaga.setIcon(new ImageIcon(this.getClass().getResource("nikujyaga.jpg") ));
        button_gyudon.setIcon(new ImageIcon(this.getClass().getResource("gyudon.jpg") ));
        button_karaage.setIcon(new ImageIcon(this.getClass().getResource("karaage.jpg") ));

    button_shougayaki.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Shougayaki", 700);
        }
    });

    button_hamburg.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Hamburg", 800);
        }
    });
    button_curry.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Curry", 900);
        }
    });
    button_nikujyaga.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Nikujyaga", 1000);
        }
    });
    button_gyudon.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Gyudon", 1100);
        }
    });
    button_karaage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            order("Karaage", 1200);
        }
    });
    checkoutbutton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            int confirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to checkout?",
                    "Checkout Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if(confirmation == 0) {
                JOptionPane.showMessageDialog(
                        null,
                        "Thank you. The total price is " + totalprice + " yen."
                );
                orderedItemsList.setText("");
                totalprice = 0;
                pricefield.setText(totalprice + "yen");
            }
        }
    });

    }



    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMashineGUI");
        frame.setContentPane(new FoodOrderingMashineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            int oomoriconfirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like a large serving of rice? (+200yen) ",
                    "Rice Size Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible."
            );
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food + " " + price + "yen\n");
            totalprice = totalprice + price;
            if(oomoriconfirmation == 0){
                totalprice = totalprice + 200;
                String currentText1 = orderedItemsList.getText();
                orderedItemsList.setText(currentText1 + "  large rice  +200yen\n");
            }
            pricefield.setText(totalprice + "yen");
        }
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
